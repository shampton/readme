# Weekly Priorities

This communicates to the 2 to 3 big rocks I am trying to accomplish this week usually in the style of OKRs with the heading being an objective and bullet points being key results to accomplish that. It will be updated weekly.

### Legend

These designations are added to the previous week's priority list when adding the current week's priority.

- Y - Completed
- N - Not

## 2023-01-23

- Setup UX benchmark projects for PA and PE
- Manage a couple recent customer support issues
- Begin writing a coverage document for when I'm on parental leave

## 2022-12-12

- Update issue board planning breakdown column with proper stack ranking **Y**
- Update team retrospective issue template **Y**
- Begin on-call schedule preparation for January **Y**

## 2022-12-05

- Handle the high priority support requests **Y**
- Update issue board with prioritized stack ranking now that we have moved to our new workflow system **Y**
- Review and update talent assessments **Y**

## 2022-11-07

- Plan 15.7 milestone **Y**
- Update documentation for artifact endpoints **Y**
- OpenAPI documentation for endpoints **Y**

## 2022-10-31

- Wrap up all talent assessments for my reports and myself **Y**
- Update documentation for artifact endpoints **N (MR is in review)**
- OpenAPI documentation for endpoints **N (one MR in review)**

## 2022-10-24

- Talent assessments for my reports and myself **N**
- Updating the Pipeline Insights Group handbook page (editing, cleaning old sections, etc.) **Y**
- Researching the validity of many of our documented artifacts API endpoints **Y**
