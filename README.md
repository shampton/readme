# Scott's README

My name is Scott Hampton and I'm the Fullstack Engineering Manager for the Verify:Pipeline Insights group at GitLab.

- [LinkedIn](https://www.linkedin.com/in/scott-hampton-dev)
- [Twitter](https://twitter.com/iamshampton)
- [GitLab](https://gitlab.com/shampton)
- [GitLab Team Page](https://about.gitlab.com/company/team/#shampton)

This page is intended to state how I like/strive to work with others, and help others know what it is like to work with me.

## Personally

- I was born and raised in Utah, but now live in Arizona.
- My wife and I moved to Arizona, and we have one child.
- We also have two cats, [Albus](https://about.gitlab.com/company/team-pets/#92-albus) and [Harry](https://about.gitlab.com/company/team-pets/#93-harry).
- I graduated from Brigham Young University with a bachelor's degree in Computer Science.
- My [Social Style](https://about.gitlab.com/handbook/leadership/emotional-intelligence/social-styles/) is technically Analytical, but I'm pretty close to center of the graph.
- I play a lot of music - primarily indie rock/pop. Check out [my bandcamp page](https://capecoyote.bandcamp.com/) if you would like to hear home-produced music.

## Professionally

### Professional History

- I started working at GitLab at the end of October 2018.
- I was hired to work on [the Verify team](https://about.gitlab.com/handbook/engineering/development/ops/verify/), which has now been split into a few separate teams to where now I work on [the Verify:Pipeline Insights group](https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-insights/).
- I was hired as a frontend engineer, but am now working as the engineering manager for [the Verify:Pipeline Insights group](https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-insights/).
- Prior to GitLab, I worked at [Carvana](https://www.carvana.com/) and [BlenderBottle](https://www.blenderbottle.com/).

### How I like to work

- I believe in the [servant-leader](https://en.wikipedia.org/wiki/Servant_leadership) model. As a manager, my job is to make you most effective.
- GitLab is a large company and there's a lot of information, issues, MRs, news, etc. happening all the time. I do my best to help filter these things to what matters most to my team, so that they can focus on the work they enjoy.
- I prefer to listen more than speak. If you feel like I am not participating in a group meeting, please ask me a question directly and I'll be happy to answer.
- When you need me to respond either on GitLab or Slack, please ping me directly. My notifications are based on when people ping me.
- More often than not, I am coming into an issue with low context. Overcommunicating the context of the issue when pinging me helps tremendously.
- I'm not afraid to say when I don't know something, and there's a lot I don't know/understand. Please be patient with me, and help me understand so that I can help you better.
- I'm generally quick to respond on GitLab, but please give me at least 24 hours (weekday) to respond before pinging me again.
- If I determine that I don't have enough time at the moment to give proper attention to your request, I'll let you know quickly in case you would like to involve someone else in the meantime.

